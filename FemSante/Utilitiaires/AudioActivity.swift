//
//  AudioActivity.swift
//  FemSante
//
//  Created by utente on 02/04/24.
//

import SwiftUI
import AVKit

struct AudioActivity: View {
    
    let title : String
    
    var dictionnary : Array<String>
    
    @State var choix : String
    
    init(title: String, dictionnary: Array<String>) {
        self.title = title
        self.dictionnary = dictionnary
        self.choix = dictionnary.first!
    }
    
    var body: some View {
        let url = Bundle.main.url(forResource: choix, withExtension: "mp4")
        let player = AVPlayer(url: url!)
        VStack (spacing : 30) {
            Text(title)
                .font(Font.custom("Nothing You Could Do", size: 36))
                .multilineTextAlignment(.center)
                .padding(.horizontal, 100.0)
            HStack {
                Text("Choisis un fichier audio".uppercased())
                    .font(Font.custom("DINPro", size: 20))
                Picker("Recettes", selection: $choix, content: {
                ForEach( dictionnary, id:\.self) { value in
                    Text(value)}.pickerStyle(.menu)
            })
            }
            if url != nil {
                VideoPlayer(player: player).scaledToFit().onChange(of: choix) { _ in
                    player.pause()
                    player.replaceCurrentItem(with: AVPlayerItem(url: url!))
                }
            }
            else {
                Text("Video not found").font(Font.custom("Nothing You Could Do", size : 20))
            }
            Spacer()
        }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
            .background(Image("fond_appli").resizable())
    }
}

struct AudioActivity_Previews: PreviewProvider {
    static var previews: some View {
        AudioActivity(title: "Méditations", dictionnary: [
        "Calmer la colère", "Calmer la douleur", "Confiance en soi", "Relaxation"
        ])
    }
}
