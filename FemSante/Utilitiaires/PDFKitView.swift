//
//  PDFViewer.swift
//  FemSante
//
//  Created by utente on 18/03/24.
//

import SwiftUI
import PDFKit

struct PDFKitView: UIViewRepresentable {
    
    let pdfDocument: PDFDocument
    
    init(showing pdfDocument: PDFDocument) {
        self.pdfDocument = pdfDocument
    }
    
    func makeUIView(context: Context) -> PDFView {
        let pdfView = PDFView()
        pdfView.document = pdfDocument
        pdfView.autoScales = true
        
        return pdfView
    }
    
    func updateUIView(_ pdfView : PDFView, context: Context)
    {
        pdfView.document = pdfDocument
    }
    
}

struct PDFViewer: View {
    
    let pdfDoc: PDFDocument
    
    init(destination : String) {
        let url = Bundle.main.url(forResource: destination, withExtension: "pdf")
        self.pdfDoc = PDFDocument(url: url!)!
    }
    
    var body: some View {
        PDFKitView(showing: pdfDoc)
    }
}
