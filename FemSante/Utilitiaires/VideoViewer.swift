//
//  VideoViewer.swift
//  FemSante
//
//  Created by utente on 27/03/24.
//

import SwiftUI
import AVKit

struct VideoViewer: View {
    
    let title : String
    
    @State var pdfToggle = false
    
    let pdf : Bool
    
    init(title: String, pdf : Bool = false) {
        self.title = title
        self.pdf = pdf
    }
    
    var body: some View {
        VStack (spacing : 20) {
            Text(title)
                .font(Font.custom("Nothing You Could Do", size: 36))
                .multilineTextAlignment(.center)
                .padding(.horizontal, 100.0)
            if let url = Bundle.main.url(forResource: title, withExtension: "mp4") {
                VideoPlayerControl(url: url)
                    .scaledToFit()
            }
            else {
                Text("Video not found").font(Font.custom("Nothing You Could Do", size : 20))
            }
            if pdf {
                Button {
                    pdfToggle.toggle()
                 } label: {
                     Text("PDF explicatif").font(Font.custom("DINPro", size : 20))
                 }.sheet(isPresented: $pdfToggle, content: { PDFViewer(destination: title)})
                    .padding(.all, 15.0)
                    .background(.orange)
                    .cornerRadius(10)
                    .foregroundColor(Color.black)
            }
            Spacer()
        }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
            .background(Image("fond_appli").resizable())
    }
}

struct VideoPlayerControl : UIViewControllerRepresentable {
    
    var url : URL
    
    init(url: URL) {
        self.url = url
    }
    
    func makeUIViewController(context: Context) -> some UIViewController {
        let controller = AVPlayerViewController()
        let player = AVPlayer(url: url)
        
        controller.player = player
        
        return controller
    }
    
    func updateUIViewController(_ uiViewController: UIViewControllerType, context: Context) {
        
    }
    
}

struct VideoViewer_Previews: PreviewProvider {
    static var previews: some View {
        VideoViewer(title : "Joie", pdf: true).previewInterfaceOrientation(.landscapeLeft)
    }
}
