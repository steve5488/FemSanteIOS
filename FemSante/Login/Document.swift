//
//  Document.swift
//  FemSante
//
//  Created by utente on 13/03/24.
//

import SwiftUI

struct Document: View {
    var body: some View {
        NavigationStack {
            VStack (spacing : 50){
                Text("Documents")
                    .font(Font.custom("Nothing You Could Do", size: 36))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 100.0)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "Conditions générales d'utilisation"), label: {
                        Text("Conditions générales d'utilisation").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "Conditions générales de vente"), label: {
                        Text("Conditions générales de vente").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "Mentions légales"), label: {
                        Text("Mentions légales").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "Politiques de confidentialité"), label: {
                        Text("Politiques de confidentialité").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Spacer()
            }.background(Image("fond_appli").resizable())
        }}
}

struct Document_Previews: PreviewProvider {
    static var previews: some View {
        Document()
    }
}
