//
//  Login.swift
//  FemSante
//
//  Created by utente on 13/03/24.
//

import SwiftUI

struct Login: View {
    
    init() {
        UITabBar.appearance().backgroundColor = UIColor(red: 0/255, green: 133/255, blue: 172/255, alpha: 1.0)
        UITabBar.appearance().unselectedItemTintColor = .darkGray
    }
    
    var body: some View {
        NavigationStack {
            TabView {
                Connexion().tabItem {
                    Text("Connexion")
                    Image(systemName: "person.circle.fill")
                }
                Inscription().tabItem {
                    Text("Inscription")
                    Image(systemName: "pencil")
                }
                Document().tabItem {
                    Text("Documents")
                    Image(systemName: "doc.fill")
                }
            }
        }
    }
}

struct Login_Previews: PreviewProvider {
    static var previews: some View {
        Login()
    }
}
