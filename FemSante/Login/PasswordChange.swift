//
//  PasswordChange.swift
//  FemSante
//
//  Created by utente on 18/03/24.
//

import SwiftUI


struct PasswordChange: View {
    
    @Environment(\.presentationMode) var presentationMode
    
    @State private var username : String = ""
    @State private var password : String = ""
    @State private var confirm : String = ""
    @State private var answer : String = ""

    @State private var selection : String = "Cliquez pour sélectionner une question"
    private var questions = ["Nom de jeune fille de votre mère", "Nom de votre 1er animal de compagnie", "Prénom de votre meilleur(e) ami(e) d'enfance"]
    
    var body: some View {
        VStack(alignment: .center, spacing: 20) {
            Text("Mot de passe oublié")
                .font(Font.custom("Nothing You Could Do", size: 36))
            TextField("Veuillez saisir votre e-mail", text: $username)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            TextField("Veuillez saisir votre mot de passe", text : $password)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            TextField("Confirmez votre mot de passe", text : $confirm)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            Picker ("Cliquez pour sélectionner une question", selection: $selection ) {
                ForEach(questions, id: \.self) {
                    Text($0)
                }.pickerStyle(.menu)
            }
            TextField("Réponse à la question secrète", text : $answer)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            Button {
                presentationMode.wrappedValue.dismiss()
            } label: {
                Text("Mot de passe oublié?").font(Font.custom("DINPro", size : 20))
            }
            .padding(.all, 11.0)
            .background(.orange)
            .cornerRadius(10)
            .foregroundColor(Color.black)
            Spacer()
        }
    }
}

struct PasswordChange_Previews: PreviewProvider {
    static var previews: some View {
        PasswordChange()
    }
}
