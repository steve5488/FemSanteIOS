//
//  Inscription.swift
//  FemSante
//
//  Created by utente on 06/03/24.
//

import SwiftUI

struct Inscription: View {
    
    @State private var name : String = ""
    @State private var username : String = ""
    @State private var password : String = ""
    @State private var confirm : String = ""
    @State private var answer : String = ""
    
    @State private var selection : String = "Nom de jeune fille de votre mère"
    private var questions = ["Nom de jeune fille de votre mère", "Nom de votre 1er animal de compagnie", "Prénom de votre meilleur(e) ami(e) d'enfance"]
    
    var body: some View {
        ScrollView {
            VStack (alignment: .center) {
                HStack {
                    Text("Inscription")
                        .font(Font.custom("Nothing You Could Do", size: 36))
                }
                .padding(.horizontal, 110.0)
                VStack (alignment: .center){
                    TextField("Saisissez votre prénom", text: $name)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding()
                    TextField("Saisissez votre e-mail", text : $username)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding()
                    TextField("Saisissez votre mot de passe", text: $password)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding()
                    Text("Le mot de passe est composé d'au moins 8 caractères dont au moins 1 majuscule, 1 caractère spécial et 1 minuscule")
                        .padding()
                        .multilineTextAlignment(.center)
                    TextField("Confirmez votre mot de passe", text : $confirm)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding()
                    HStack {
                        Text("Question secrète".uppercased())
                            .padding()
                            .font(Font.custom("DINPro", size: 20))
                        Picker ("Cliquez pour sélectionner une question", selection: $selection ) {
                            ForEach(questions, id: \.self) {
                                Text($0)
                            }.pickerStyle(.menu)
                        }
                    }
                    TextField("Réponse à la question secrète", text : $answer)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding()
                    Button {
                        /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                    } label: {
                        Text("Payer l'inscription").font(Font.custom("DINPro", size : 20))
                    }
                    .padding(.all, 11.0)
                    .background(.orange)
                    .cornerRadius(10)
                    .foregroundColor(Color.black)
                    Button {
                        /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                    } label: {
                        Text("Essai gratuit de 7 jours").font(Font.custom("DINPro", size : 20))
                    }
                    .padding(.all, 11.0)
                    .background(.orange)
                    .cornerRadius(10)
                    .foregroundColor(Color.black)
                }
            }
            Spacer()
        }
    }
}

struct Inscription_Previews: PreviewProvider {
    static var previews: some View {
        Inscription()
    }
}
