//
//  ContentView.swift
//  FemSante
//
//  Created by utente on 21/02/24.
//

import SwiftUI

struct Connexion: View {
    
    @State private var username : String = ""
    @State private var password : String = ""
    @State private var connectSuccess : Bool = false
    @State private var forgottenChange = false
    
    var body: some View {
        NavigationStack {
            VStack(alignment : .center, spacing : 50) {
                HStack {
                    Text("Connexion")
                        .font(Font.custom("Nothing You Could Do", size: 36))
                }
                .padding(.horizontal, 110.0)
                VStack (alignment: .leading){
                    TextField("Veuillez saisir votre e-mail", text: $username)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding()
                    TextField("Veuillez saisir votre mot de passe", text : $password)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                        .padding()
                }
                Button {
                    connectSuccess.toggle()
                } label: {
                    NavigationLink(destination: Main(), label: {
                        Text("Connexion").font(Font.custom("DINPro", size : 20))})
                    // this sets the Back button text when a new screen is pushed
                    .navigationTitle("Déconnexion")
                    .toolbar(.hidden)
                }
                    .padding(.all, 11.0)
                    .background(.orange)
                    .cornerRadius(10)
                    .foregroundColor(Color.black)
                Button {
                    forgottenChange.toggle()
                } label: {
                    NavigationLink(destination: PasswordChange(), label: {
                        Text("Mot de passe oublié ?").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 11.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Spacer()
            }
            .background(Image("fond_appli").resizable())
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Connexion()
    }
}
