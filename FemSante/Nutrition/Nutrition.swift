//
//  Nutrition.swift
//  FemSante
//
//  Created by utente on 18/03/24.
//

import SwiftUI

struct Nutrition: View {
    
    init() {
        UITabBar.appearance().backgroundColor = UIColor(red: 0/255, green: 133/255, blue: 172/255, alpha: 1.0)
        UITabBar.appearance().unselectedItemTintColor = .darkGray
    }
    
    var body: some View {
        TabView {
            NutriMenu().tabItem {
                Text("Recettes")
                Image(systemName: "menucard.fill")
            }
            NutriDocs().tabItem {
                Text("Ressources nutrition")
                Image(systemName: "doc.fill")
            }
        }
    }
}

struct Nutrition_Previews: PreviewProvider {
    static var previews: some View {
        Nutrition()
    }
}
