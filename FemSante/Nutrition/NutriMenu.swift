//
//  NutritionMenu.swift
//  FemSante
//
//  Created by utente on 18/03/24.
//

import SwiftUI

struct NutriMenu: View {
    var body: some View {
        NavigationStack {
            VStack (alignment: .center, spacing: 30) {
                Text("Nutrition")
                    .font(Font.custom("Nothing You Could Do", size: 36))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 100.0)
                Button {
                    
                } label: {
                    NavigationLink(destination: Recipes(title : "Petit-déjeuner", recipes : ["bf1" : "Pain de lentilles corail et oeufs brouillés",
                                                                                       "bf2" : "Porridges salé aux amandes"]), label: {
                        Text("Petit-déjeuner").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: Recipes(title : "Entrées", recipes :
                                                            ["ent1" : "Salade d’été",
                                                             "ent2" : "Tartines gourmandes au thon",
                                                             "ent3" : "Velouté d’épinards, amandes et noisettes",
                                                             "ent4" : "Blinis avocat saumon",
                                                             "ent5" : "Houmous de betteraves"]), label: {
                        Text("Entrées").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                } label: {
                    NavigationLink(destination: Recipes(title : "Plats", recipes :
                                                            ["plat1" : "Crêpes salées au houmous rose",
                                                             "plat2" : "Tartelettes saumon et champignons",
                                                             "plat3" : "Cake salé",
                                                             "plat4" : "Polenta aux champignons",
                                                             "plat5" : "Tofu fumé - Purée de carottes et panais",
                                                             "plat6" : "Tarte aux épinards"]), label: {
                        Text("Plats").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {} label: {
                    NavigationLink(destination: Recipes(title : "Plats", recipes :
                                                            ["des1" : "Petits cakes vapeur pommes-myrtilles",
                                                             "des2" : "Crème au chocolat",
                                                             "des3" : "Fondant au chocolat",
                                                             "des4" : "Tartelettes à la pomme express",
                                                             "des5" : "Cake au chocolat léger et sans farines",
                                                             "des6" : "Carrés gourmands",
                                                             "des7" : "Mini cake moelleux banane et chocolat"]), label: {
                        Text("Desserts").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Spacer()
            }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
                .background(Image("fond_appli").resizable())
        }
    }
}

struct NutritionMenu_Previews: PreviewProvider {
    static var previews: some View {
        NutriMenu()
    }
}
