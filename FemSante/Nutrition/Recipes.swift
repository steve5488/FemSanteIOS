//
//  Recipes.swift
//  FemSante
//
//  Created by utente on 19/03/24.
//

import SwiftUI
import OrderedCollections

struct Recipes: View {
    
    func findKey(search : String, dict: OrderedDictionary<String, String>) -> String {
        
        for (key, value) in dict {
            if value == search {
                return key
            }
        }
        return "1"
    }
    
    var title = ""
    
    @State var recipeOn = false
    
    @State var choix : String
    
    var dictionnary : OrderedDictionary<String, String>
    
    init(title: String, recipes : OrderedDictionary<String, String>) {
        self.title = title
        self.dictionnary = recipes
        self.choix = recipes.elements[0].key
    }
    
    var body: some View {
        NavigationStack {
            VStack(alignment: .center, spacing: 30) {
                Text(title)
                    .font(Font.custom("Nothing You Could Do", size: 36))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 100.0)
                HStack {
                    Text("Liste des recettes".uppercased())
                        .padding()
                        .font(Font.custom("DINPro", size: 25))
                    Picker("Recettes", selection: $choix, content: {
                        ForEach( dictionnary.elements, id:\.key) { key, value in
                            Text(value)}.pickerStyle(.menu)
                    })
                }
                Button {
                    recipeOn.toggle()
                 } label: {
                     Image(choix).resizable().scaledToFit().frame(height: 300)
                 }.sheet(isPresented: $recipeOn, content: { PDFViewer(destination: choix)})
                Text("Cliquez sur l'image pour afficher la recette")
                    .font(Font.custom("DINPro", size: 15))
                Spacer()
            }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
                .background(Image("fond_appli").resizable())
                .onAppear() {
                }
        }
    }
}

struct Recipes_Previews: PreviewProvider {
    static var previews: some View {
        Recipes(title : "Recettes", recipes : ["bf1" : "Pain de lentilles corail et oeufs brouillés",
                                               "bf2" : "Porridges salé aux amandes"])
    }
}
