//
//  NutriDocs.swift
//  FemSante
//
//  Created by utente on 18/03/24.
//

import SwiftUI

struct NutriDocs: View {
    
    @State var image = Image("fond_appli")
    
    var body: some View {
        NavigationStack {
            VStack (alignment: .center, spacing: 40) {
                Text("Ressources nutrition")
                    .font(Font.custom("Nothing You Could Do", size: 36))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 100.0)
                Button {} label: {
                    NavigationLink(destination: PDFViewer(destination: "Sensibilité au gluten"), label: {
                        Text("Sensibilité au gluten").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {} label: {
                    NavigationLink(destination: PDFViewer(destination: "Intolérance à l'histamine"), label: {
                        Text("Intolérance à l'histamine").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "Les secrets des micronutriments"), label: {
                        Text("Les secrets des micronutriments").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Spacer()
            }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
                .background(image.resizable())
        }
    }
}

struct NutriDocs_Previews: PreviewProvider {
    static var previews: some View {
        NutriDocs()
    }
}
