//
//  ArtThérapie.swift
//  FemSante
//
//  Created by utente on 27/03/24.
//

import SwiftUI

struct ArtTherapie: View {
    var body: some View {
        NavigationStack {
            VStack (spacing : 50){
                Text("Art-Thérapie")
                    .font(Font.custom("Nothing You Could Do", size: 36))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 100.0)
                Button {} label: {
                    NavigationLink(destination: VideoViewer(title: "Colère", pdf: true), label: {
                        Text("Colère").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {} label: {
                    NavigationLink(destination: VideoViewer(title: "Joie", pdf: true), label: {
                        Text("Joie").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {} label: {
                    NavigationLink(destination: VideoViewer(title: "Peur", pdf: true), label: {
                        Text("Peur").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {} label: {
                    NavigationLink(destination: VideoViewer(title: "Tristesse", pdf: true), label: {
                        Text("Tristesse").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Spacer()
            }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
                .background(Image("fond_appli").resizable())
        }    }
}

struct ArtTherapie_Previews: PreviewProvider {
    static var previews: some View {
        ArtTherapie()
    }
}
