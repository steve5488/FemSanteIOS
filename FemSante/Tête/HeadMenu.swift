//
//  HeadMenu.swift
//  FemSante
//
//  Created by utente on 27/03/24.
//

import SwiftUI

struct HeadMenu: View {
    var body: some View {
        NavigationStack {
            VStack (spacing : 50){
                Text("Bien dans ta tête")
                    .font(Font.custom("Nothing You Could Do", size: 36))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 100.0)
                Button {
                } label: {
                    NavigationLink(destination: AudioActivity(title: "Méditations", dictionnary: [
                        "Calmer la colère", "Calmer la douleur", "Confiance en soi", "Relaxation"
                        ]), label: {
                        Text("Méditations").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink (destination: VideoViewer(title: "Intelligence émotionnelle") , label: {
                        Text("Intelligence émotionnelle ").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                } label: {
                    NavigationLink (destination: Sophrologie() , label: {
                        Text("Sophrologie ").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {} label: {
                    NavigationLink(destination: ArtTherapie(), label: {
                        Text("Art-thérapie").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {} label: {
                    NavigationLink(destination: AudioActivity(title: "Hypnose", dictionnary: [
                        "Auto hypnose pour le stress", "Auto-hypnose pour l'apaisement"]), label: {
                        Text("Hypnose").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Spacer()
            }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
                .background(Image("fond_appli").resizable())
        }
    }
}

struct HeadMenu_Previews: PreviewProvider {
    static var previews: some View {
        HeadMenu()
    }
}
