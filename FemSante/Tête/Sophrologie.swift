//
//  Sophrologie.swift
//  FemSante
//
//  Created by utente on 27/03/24.
//

import SwiftUI

struct Sophrologie: View {
    var body: some View {
        NavigationStack {
            VStack (spacing : 50){
                Text("Sophrologie")
                    .font(Font.custom("Nothing You Could Do", size: 36))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 100.0)
                Button {
                } label: {
                    NavigationLink (destination: VideoViewer(title: "Exercice du miroir") , label: {
                        Text("Exercice du miroir").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink (destination: VideoViewer(title: "Eventails") , label: {
                        Text("Eventails").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                } label: {
                    NavigationLink (destination: VideoViewer(title: "Pompage des épaules") , label: {
                        Text("Pompage des épaules").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {} label: {
                    NavigationLink (destination: VideoViewer(title: "Soufflet thoracique") , label: {
                        Text("Soufflet thoracique").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {} label: {
                    NavigationLink(destination: ToolBox(), label: {
                        Text("Sophronisations").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Spacer()
            }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
                .background(Image("fond_appli").resizable())
        }
    }
}

struct Sophrologie_Previews: PreviewProvider {
    static var previews: some View {
        Sophrologie()
    }
}
