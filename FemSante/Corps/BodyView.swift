//
//  BodyView.swift
//  FemSante
//
//  Created by utente on 02/04/24.
//

import SwiftUI

struct BodyView: View {
    var body: some View {
        NavigationStack {
            VStack (spacing : 50){
                Text("Bien dans ta tête")
                    .font(Font.custom("Nothing You Could Do", size: 36))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 100.0)
                Button {
                } label: {
                    NavigationLink(destination: Yoga(), label: {
                        Text("Méditations").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {} label: {
                    NavigationLink (destination: VideoViewer(title: "Pilates") , label: {
                        Text("Pilates").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                } label: {
                    NavigationLink (destination: VideoViewer(title: "Fitness") , label: {
                        Text("Fitness").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Spacer()
            }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
                .background(Image("fond_appli").resizable())
        }
    }
}

struct BodyView_Previews: PreviewProvider {
    static var previews: some View {
        BodyView()
    }
}
