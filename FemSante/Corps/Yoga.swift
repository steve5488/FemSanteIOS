//
//  Yoga.swift
//  FemSante
//
//  Created by utente on 02/04/24.
//

import SwiftUI

struct Yoga: View {
    var body: some View {
        VStack (spacing : 50){
            Text("Yoga")
                .font(Font.custom("Nothing You Could Do", size: 36))
                .multilineTextAlignment(.center)
                .padding(.horizontal, 100.0)
            Button {
            } label: {
                NavigationLink(destination: VideoViewer(title: "Débutant au Yoga"), label: {
                    Text("Méditations").font(Font.custom("DINPro", size : 20))
                })
            }
            .padding(.all, 15.0)
            .background(.orange)
            .cornerRadius(10)
            .foregroundColor(Color.black)
            Button {} label: {
                NavigationLink (destination: VideoViewer(title: "Retrouver son calme intérieur") , label: {
                    Text("Retrouver son calme intérieur").font(Font.custom("DINPro", size : 20))})
            }
            .padding(.all, 15.0)
            .background(.orange)
            .cornerRadius(10)
            .foregroundColor(Color.black)
            Button {
            } label: {
                NavigationLink (destination: VideoViewer(title: "SOS Douleur") , label: {
                    Text("SOS Douleur").font(Font.custom("DINPro", size : 20))})
            }
            .padding(.all, 15.0)
            .background(.orange)
            .cornerRadius(10)
            .foregroundColor(Color.black)
            Spacer()
        }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
        .background(Image("fond_appli").resizable())
    }
}

struct Yoga_Previews: PreviewProvider {
    static var previews: some View {
        Yoga()
    }
}
