//
//  Main.swift
//  FemSante
//
//  Created by utente on 13/03/24.
//

import SwiftUI

struct Main: View {
    
    init() {
        UITabBar.appearance().backgroundColor = UIColor(red: 0/255, green: 133/255, blue: 172/255, alpha: 1.0)
        UITabBar.appearance().unselectedItemTintColor = .darkGray
    }
    
    var body: some View {
        TabView {
            MainView().tabItem {
                Text("Menu principal")
                Image(systemName: "list.bullet.circle.fill")
            }
            Account().tabItem {
                Text("Mon compte")
                Image(systemName: "person.crop.circle.badge.exclamationmark")
            }
        }
    }
}

struct Main_Previews: PreviewProvider {
    static var previews: some View {
        Main()
    }
}
