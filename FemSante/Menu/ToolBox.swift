//
//  ToolBox.swift
//  FemSante
//
//  Created by utente on 18/03/24.
//

import SwiftUI

struct ToolBox: View {
    var body: some View {
        NavigationStack {
            VStack (spacing : 20){
                Text("Boîte à outils")
                    .font(Font.custom("Nothing You Could Do", size: 36))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 100.0)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "Automassage du ventre"), label: {
                        Text("Automassage du ventre").font(Font.custom("DINPro", size : 20)).multilineTextAlignment(.center)
                    })
                }
                .padding(.all, 10.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "bouillote"), label: {
                        Text("Les vertus de la bouillote ").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "douleurs_abdominales"), label: {
                        Text("Huiles essentielles pour apaiser les douleurs abdominales").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 10.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "emotional_tempest_oil"), label: {
                        Text("Huiles essentielles pour apaiser les tempêtes emotionelles").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 10.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "emotional_tempest"), label: {
                        Text("Infusions pour apaiser les tempêtes emotionelles").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 10.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "infusion_digestion"), label: {
                        Text("Infusions et digestions").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 10.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "infusions_menstruations"), label: {
                        Text("Infusions pour apaiser les menstruations").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Spacer()
            }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
            .background(Image("fond_appli").resizable())}
    }
}

struct ToolBox_Previews: PreviewProvider {
    static var previews: some View {
        ToolBox()
    }
}
