//
//  MainView.swift
//  FemSante
//
//  Created by utente on 13/03/24.
//

import SwiftUI

struct MainView: View {
    
    @State var nutriMenu = false
    @State var headMenu = false
    @State var bodyMenu = false
    @State var toolBoxMenu = false
    
    var body: some View {
        NavigationStack {
            VStack (spacing : 50){
                Text("Menu principal")
                    .font(Font.custom("Nothing You Could Do", size: 36))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 100.0)
                Button {
                    nutriMenu.toggle()
                } label: {
                    NavigationLink(destination: Nutrition(), label: {
                        Text("Nutrition").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: HeadMenu(), label: {
                        Text("Bien dans ta tête").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                } label: {
                    Text("Bien dans ton corps").font(Font.custom("DINPro", size : 20))
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    toolBoxMenu.toggle()
                } label: {
                    NavigationLink(destination: ToolBox(), label: {
                        Text("Boîte à outils").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Spacer()
            }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
                .background(Image("fond_appli").resizable())
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
