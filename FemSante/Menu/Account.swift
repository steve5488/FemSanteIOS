//
//  Account.swift
//  FemSante
//
//  Created by utente on 13/03/24.
//

import SwiftUI

struct Account: View {
    var body: some View {
        ScrollView {
            VStack (spacing : 20){
                Text("Mon compte")
                    .font(Font.custom("Nothing You Could Do", size: 36))
                    .multilineTextAlignment(.center)
                    .padding(.horizontal, 100.0)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PasswordChange(), label: {
                        Text("Mot de passe oublié ?").font(Font.custom("DINPro", size : 20))})
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "Conditions générales d'utilisation"), label: {
                        Text("Conditions générales d'utilisation").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "Conditions générales de vente"), label: {
                        Text("Conditions générales de vente").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "Mentions légales"), label: {
                        Text("Mentions légales").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    NavigationLink(destination: PDFViewer(destination: "Politiques de confidentialité"), label: {
                        Text("Politiques de confidentialité").font(Font.custom("DINPro", size : 20))
                    })
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Button {
                    /*@START_MENU_TOKEN@*//*@PLACEHOLDER=Action@*/ /*@END_MENU_TOKEN@*/
                } label: {
                    Text("Mise à jour de l'abonnement").font(Font.custom("DINPro", size : 20))
                }
                .padding(.all, 15.0)
                .background(.orange)
                .cornerRadius(10)
                .foregroundColor(Color.black)
                Spacer()
            }
        }.frame(maxWidth: UIScreen.main.bounds.width,  maxHeight:UIScreen.main.bounds.height, alignment: .center)
            .background(Image("fond_appli").resizable())
    }
}

struct Account_Previews: PreviewProvider {
    static var previews: some View {
        Account()
    }
}
